import ipaddress

from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    get_host_vars,
)
from django_cdstack_models.django_cdstack_models.models import CmdbHost


def handle(zipfile_handler, template_opts, cmdb_host: CmdbHost):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_intrt/django_cdstack_tpl_intrt"

    if "network_route_announce_list" not in template_opts:
        template_opts["network_route_announce_list"] = list()

    if "network_route_announce_peer_list" not in template_opts:
        template_opts["network_route_announce_peer_list"] = list()

    network_route_announce_peer_groups = cmdb_host.cmdbhostgroups_set.filter(
        group_rel__config_pkg="intrt"
    )

    for peer_group in network_route_announce_peer_groups:
        peer_group_hosts = CmdbHost.objects.filter(
            cmdbhostgroups__group_rel=peer_group.group_rel_id
        ).exclude(id=cmdb_host.id)

        for peer_host in peer_group_hosts:
            peer_host_template = get_host_vars(peer_host)

            if (
                "network_route_announce_peer_ip" in peer_host_template
                and "network_route_announce_peer_as" in peer_host_template
            ):
                route_peer = dict()
                route_peer["node_hostname_fqdn"] = peer_host_template[
                    "node_hostname_fqdn"
                ]
                route_peer["network_route_announce_peer_ip"] = peer_host_template[
                    "network_route_announce_peer_ip"
                ]
                route_peer["network_route_announce_peer_as"] = peer_host_template[
                    "network_route_announce_peer_as"
                ]

                template_opts["network_route_announce_peer_list"].append(route_peer)

    for key in template_opts.keys():
        if key.startswith("network_route_announce_") and key.endswith("_name"):
            key_ident = key[:-5]

            net_var = "network_iface_" + template_opts[key_ident + "_iface"]

            net_cidr4 = ipaddress.ip_network(
                template_opts[net_var + "_ip"]
                + "/"
                + template_opts[net_var + "_netmask"],
                strict=False,
            )
            net_cidr6 = ipaddress.ip_network(
                template_opts[net_var + "_ip6"]
                + "/"
                + template_opts[net_var + "_netmask6"],
                strict=False,
            )

            announce_conf = dict()
            announce_conf["announce_name"] = template_opts[key_ident + "_name"]
            announce_conf["announce_descr"] = template_opts[key_ident + "_descr"]
            announce_conf["announce_iface"] = template_opts[key_ident + "_iface"]
            announce_conf["announce_cidr4"] = str(net_cidr4)
            announce_conf["announce_cidr6"] = str(net_cidr6)

            template_opts["network_route_announce_list"].append(announce_conf)

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
